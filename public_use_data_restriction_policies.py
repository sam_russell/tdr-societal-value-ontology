import os
from typing import Dict, List

from lxml import etree
import pandas as pd

DATA_DIR = "re3data_records_xml"

NAMESPACES = {
    'r3d': "http://www.re3data.org/schema/2-2",
    'xsi': "http://www.w3.org/2001/XMLSchema-instance"
}


def make_etree(file_path: str):
    try:
        with open(file_path, 'r') as f:
            try:
                data = f.read()
                t = etree.fromstring(data.encode('utf8'))
                for k, v in NAMESPACES.items():
                    t.nsmap[k] = v
                return t
            except Exception as e:
                print(e)
    except Exception:
        print(os.getcwd())
        print(file_path)
        return 0


def load_files(data_dir: str):
    paths = [os.path.join(data_dir, f) for f in os.listdir(data_dir)]
    #print(paths)
    return [t for t in [
        make_etree(f) for f in paths
    ] if t]


def policy_queries(tree):
    accumulator = []
    for e in tree.iter():
        print(e)
        if etree.QName(e).localname == "policyName":
            accumulator.append(e)
        elif e.iterchildren():
            for i in e.iterchildren():
                policy_queries(i)
        else:
            continue
    [i.text for i in accumulator]


def queries(tree, elemIdentifier):
    accumulator = []
    for e in tree.iter():
        print(e)
        if etree.QName(e).localname == elemIdentifier:
            accumulator.append(e)
        elif e.iterchildren():
            for i in e.iterchildren():
                queries(i, elemIdentifier)
        else: print("done")
    return [i.text for i in accumulator]


#def query_
 

##                
  #  tag = tree.xpath('./r3d:policyName', namespaces = NAMESPACES)
   # print(tag)
    #return tree.find("r3d:policyName")
          

if __name__ == "__main__":
    data = load_files(DATA_DIR)
    # just a test
    t = data[0]
    print(queries(t, "policyName"))
