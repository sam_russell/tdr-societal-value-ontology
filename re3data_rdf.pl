:- module(re3data_rdf, [build_graph/0]).

:- use_module(library(semweb/rdf11)).
:- use_module(library(semweb/turtle)).
:- use_module(library(xpath)).
:- use_module(library(uuid)).

:- use_module(re3data).

% :- rdf_register_prefix(r3d, "http://www.re3data.org/schema/3-1")

/*
  effectively, root node is 'r3d:repository' -- OR NOT, BECAUSE WE WANT PROVENANCE METADATA TO SHOW THE SOURCE FILES


this example is for "version 3" of the schema, but the XML
files you get from re3data API are v2.  also, since we are assigning 
random identifiers to the repositories, we don't have to process 
the identifiers node separately.

for the child node identifiers:
    - it is an individual of rdf:Type 'r3d:repository',
    - we will generate a random subject IRI, its 'r3d:re3data' identifier
           will be "linked data" (ooh...)

for every other child node:
    - is an individual of rdf:Type <ns:Tag>
    + if it is a terminal node, its object is the XML element text
    - else, for each child node:
                  - it is an individual of rdf:Type <ns:ParentTag>
                  + if it is a terminal node, it has a <ns:CurrentNodeTag>
                     relation with object of the XML element text
                  - else, recur
              note:  for now, we will just use plain old literals
                     for our payload data, but later we can add 
                     procedures to search the re3data XSD to find
                     the corresponding tag and RDF literal datatypes.
                     also, extract language attributes as well.
*/

 % "http://www.re3data.org/schema/3-1"

assert_re3_rdf(Node) :-
    compound_name_arguments(Node, _, NodeData),
    NodeData = [Tag, Attrs, Content],
    uuid(Subject),
    writef("processing %w, id: %w\n", [Tag, Subject]),
    rdf_assert(Subject, rdf:type, Tag),
    assert_re3_rdf(Subject, Content).

assert_re3_rdf_(ParentId, [C0|Cn]) :-
    compound_name_arguments(C0, _, NodeData),
    NodeData = [Tag, Attrs, Content],
    (terminal_node(C0) ->
	 (Content = [Value] ->
	 rdf_assert(ParentId, Tag, Value),
	 % writef("terminal node at %w\n", [Tag]),
	 % writef("content is: %w\n", [Cn]),
	 assert_re3_rdf(ParentId, Cn)
	 ;
	 assert_re3_rdf(ParentId, Cn))
    ;
     % writef("%w is not a terminal node\n", [Tag]),
     uuid(Subject),
     rdf_assert(Subject, rdf:type, Tag),
     rdf_assert(ParentId, Tag, Subject),
     % writef("re-entering with Content arg: %w\n", [Content]),
     assert_re3_rdf(Subject, Content)),
    assert_re3_rdf(ParentId, Cn).

assert_re3_rdf(ParentId, [C0|Cn]) :-
    compound_name_arguments(C0, _, NodeData),
    NodeData = [Tag, Attrs, Content],
    (terminal_node(C0) ->
	 % writef("terminal node at %w\n", [Tag]),
	 (Content = [Value] ->
	      rdf_assert(ParentId, Tag, Value),
	      assert_re3_rdf(ParentId, Cn) ;
	  assert_re3_rdf(ParentId, Cn))
    ;
    % writef("%w is not a terminal node\n", [Tag]),
    uuid(Subject),
    rdf_assert(Subject, rdf:type, Tag),
    rdf_assert(ParentId, Tag, Subject),
    % writef("re-entering with Content arg: %w\n", [Content]),
    (assert_re3_rdf(Subject, Content) ;
     assert_re3_rdf(ParentId, Cn))).

	      
terminal_node(Element) :-
    compound_name_arguments(Element, _, ListElement),
    [_, _, Content] = ListElement,
    (Content = [] ;
     [Value] = Content,
     not(xml_is_dom(Value))).

re3_xml_to_rdf(FilePath) :-
    load_record(FilePath, [Rec]),
    assert_re3_rdf(Rec).

batch_xml_to_rdf([F0|Fn]) :-
    writef("file: %w\n", [F0]),
    re3_xml_to_rdf(F0) ; !,
    batch_xml_to_rdf(Fn).

build_graph :-
    directory_files("re3data_records_xml", [Cur,Up|Files]),
    maplist(string_concat("re3data_records_xml/"), Files, Paths),
    /*
      extracting the `r3d` XML namespace and registering 
    it as a prefix in our RDF graph		  
      */
    ([F|_] = Paths,
     load_record(F, [Rec]),
     compound_name_arguments(Rec, N, [_,Attrs,_]),
     [A|B] = Attrs, compound_name_arguments(A,_,Args),
     [XmlnsAbbrev, FQ] = Args, split_string(XmlnsAbbrev, ":", "", [_,Abbrev]),
     atom_string(Prefix, Abbrev),
     rdf_register_prefix(Prefix, FQ)),
    batch_xml_to_rdf(Paths).

build_graph_and_save(Output) :-
    build_graph,
    rdf_save(Output).


run_script :-
    get_time(T), format_time(atom(A), "%FT%T%z", T),
    atomic_list_concat([re3data_RDF_graph_, A, ".rdf"], OutPath),
    open(OutPath, write, OutStream, [create([default])]),
    build_graph, rdf_save_db(OutStream), close(OutStream).
    
    
    


