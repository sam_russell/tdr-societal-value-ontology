:- use_module(library(semweb/rdf11)).
:- use_module(library(xpath)).
:- use_module(library(uuid)).

:- use_module(re3data).

/*
  effectively, root node is 'r3d:repository' -- OR NOT, BECAUSE WE WANT PROVENANCE METADATA TO SHOW THE SOURCE FILES


this example is for "version 3" of the schema, but the XML
files you get from re3data API are v2.  also, since we are assigning 
random identifiers to the repositories, we don't have to process 
the identifiers node separately.

for the child node identifiers:
    - it is an individual of rdf:Type 'r3d:repository',
    - we will generate a random subject IRI, its 'r3d:re3data' identifier
           will be "linked data" (ooh...)

for every other child node:
    - is an individual of rdf:Type <ns:Tag>
    + if it is a terminal node, its object is the XML element text
    - else, for each child node:
                  - it is an individual of rdf:Type <ns:ParentTag>
                  + if it is a terminal node, it has a <ns:CurrentNodeTag>
                     relation with object of the XML element text
                  - else, recur
              note:  for now, we will just use plain old literals
                     for our payload data, but later we can add 
                     procedures to search the re3data XSD to find
                     the corresponding tag and RDF literal datatypes.
                     also, extract language attributes as well.
*/

assert_re3_rdf(Node) :-
    compound_name_arguments(Node, _, NodeData),
    NodeData = [Tag, Attrs, Content],
    uuid(Subject),
    rdf_assert(Subject, rdf:type, Tag),
    assert_re3_rdf(Subject, Content).

assert_re3_rdf(ParentId, [C0|Cn]) :-
    compound_name_arguments(C0, _, NodeData),
    NodeData = [Tag, Attrs, Content],
    (terminal_node(C0) ->
	 Content = [Value],
	 rdf_assert(ParentId, Tag, Value) ;
     uuid(Subject),
     rdf_assert(Subject, rdf:type, Tag),
     rdf_assert(ParentId, Tag, Subject),
     assert_re3_rdf(Subject, Content)),
    assert_re3_rdf(ParentId, Cn).
	      
terminal_node(Element) :-
    compound_name_arguments(Element, _, ListElement),
    [_, _, Content] = ListElement,
    (Content = [] ;
     [Value] = Content,
     not(xml_is_dom(Value))).


