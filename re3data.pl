:- module(re3data, [load_record/2]).

:- use_module(library(http/http_client)).
:- use_module(library(xpath)).

default_download_path(Dir) :-
    Dir = "re3data_records_xml".

make_path(Segments, Path) :-
    atomics_to_string(Segments, "/", Path).



re3_base_url(Str) :-  Str = "https://www.re3data.org".


re3_repositories_url(Str) :-
    re3_base_url(Url),
    string_concat(Url, "/api/v1/repositories", Str).


re3_repository_url(Str, Id) :-
    re3_base_url(Url),
    string_concat("/api/v1/repository/", Id, Path),
    string_concat(Url, Path, Str).


re3_repositories_get(XmlTree) :-
    re3_repositories_url(Url),
    http_get(Url, ResponseBody, []),
    open_string(ResponseBody, XmlStream),
    load_xml(XmlStream, XmlTree, [space(remove)]).


re3_repository_get_xml(DestDir, Repository) :-
    xpath(Repository, //'id'(text), Id),
    xpath(Repository, //'link'(@href), Link),
    xpath(Repository, //'name'(text), Name),

    re3_base_url(Base), string_concat(Base, Link, Url),
    http_get(Url, ResponseBody, []),

    string_concat(Id, ".xml", Filename),
    make_path([DestDir, Filename], Path),
    open(Path, write, FileDescriptor, [create([default])]),
    write(FileDescriptor, ResponseBody),
    close(FileDescriptor),
    writef("re3data.org record for %w written to %w \n", [Name, Path]).


re3_download_xml(DestDir) :-
    re3_repositories_get(RepoList),
    foreach(xpath(RepoList, //'repository', Repo),
	    re3_repository_get_xml(DestDir, Repo)).

load_record(Filepath, Record) :-
    load_xml(Filepath, Record, [space(remove)]).


run_script :-
    default_download_path(Dir),
    re3_download_xml(Dir).



