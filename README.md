# tdr-societal-value-ontology

A conceptual model for understanding the societal value of certifiably trustworthy data repositories.  The model may be used as a tool to guide further research on identifying and measuring impacts and outcomes of data stewardship activities.